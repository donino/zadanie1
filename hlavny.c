#include <stdio.h>
#include <math.h>
#include <string.h>

#define N 4

#define SAMOPRIESEK_ANO 1
#define SAMOPRIESEK_NIE 0

typedef struct PRIAMKA{
    float a, b, c;
}PRIAMKA;
        
typedef struct VEKTOR{
    float x, y;
}VEKTOR;

int zistuj(float x1, float x2, float hx, float y1, float y2, float hy ){
    float p;
    
    if(x1>x2) 
    {
        p=x1;
        x1=x2;
        x2=p;
    }
    
    if(y1>y2) 
    {
        p=y1;
        y1=y2;
        y2=p;
    }
    
    if( ((hx<x2) && (hx>x1)) && ((hy<y2) && (hy>y1)) )
        return 1;

    if( ((hx<=x2) && (hx>=x1)) && ((hy<y2) && (hy>y1)) )
        return 1;

    if( ((hx<x2) && (hx>x1)) && ((hy<=y2) && (hy>=y1)) )
        return 1;
    
    return 0;
}

void ries_rovnice(PRIAMKA *p, PRIAMKA *q, VEKTOR *vystup, char *stav){
    float d, d1, d2;
    
    
    
    d = (p->a)*(q->b) - (p->b)*(q->a);
    
    /* ak je normala jednej priamky kolma na smerovy vektor druhej, su paralelne*/
    if(d==0)
    {
        *stav='F';
        return;
    }
    
    /* system je nedegenerovany, Crammerovo pravidlo postaci*/
    d1 = -(p->c)*(q->b) + (p->b)*(q->c);
    d2 = -(p->a)*(q->c) + (p->c)*(q->a);
    
    vystup->x = d1 / d;
    vystup->y = d2 / d;
    
    return;
}

char uholnik_testuj_samoprieseky(float *x, float *y, unsigned int n){
    int i, j;
    PRIAMKA p1, p2;
    VEKTOR pr;
    char stav='T';

    for(i=0; i<(n-1); i++)
    {
        /* prevod endpointov segmentu na analyticke vyjadrenie priamky*/
        p1.a=(y[i+1] - y[i]);
        p1.b=(-1.0 * (x[i+1] - x[i]));
        p1.c= -1.0 * ((p1.a * x[i]) + (p1.b * y[i]));
        
        for(j=(i+1); j<n; j++)
        {
            p2.a=(y[j+1] - y[j]);
            p2.b=(-1.0 * (x[j+1] - x[j]));
            p2.c= -1.0 * ((p2.a * x[j]) + (p2.b * y[j]));

            /* presecnik priamok natiahnutych nad segmentami */
            ries_rovnice(&p1, &p2, &pr, &stav);
            
            if(stav=='T')
                if(zistuj(x[j], x[j+1], pr.x, y[j], y[j+1], pr.y)==1) 
                    if(zistuj(x[i], x[i+1], pr.x, y[i], y[i+1], pr.y)==1)
                        return SAMOPRIESEK_ANO;
        }
        
    }
    
    return SAMOPRIESEK_NIE;

}

int main(){
    char a;
    float s1[N+1]={3.0,1.0,3.0,1.0}, s2[N+1]={1.0,1.0,-3.0,3.0};
    
    s1[N]=s1[0];
    s2[N]=s2[0];
    
    a=uholnik_testuj_samoprieseky(s1, s2,  N);
    
    printf("\nVysledok vypoctu je: \n");
    
    if(a==1) 
    {
        printf("SAMOPRIESEK_ANO");
        return 0;
    }

    if(a==0)
        printf("SAMOPRIESEK_NIE");
    
    return 0;
}

